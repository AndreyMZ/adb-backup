# Usage

~~~
Usage: adb-backup [OPTIONS]

Options:
      --device <DEVICE>  USB device to use. Required if multiple devices are connected
      --dir <DIR>        Path to a directory containing backups
  -h, --help             Print help
  -V, --version          Print version
~~~
