use std::{fs, path::PathBuf};

use anyhow::Result;
use clap::Parser;
use env_logger;
use log::LevelFilter;
use sanitise_file_name::sanitise;
use subprocess::Exec;

use crate::adb::Package;
use crate::utils::subprocess::ExecExt;

mod adb;
mod utils;

/// This tool allows to backup applications from Android device using ADB (Android Debug Bridge).
#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// USB device to use. Required if multiple devices are connected.
    #[arg(long)] device: Option<String>,
    /// Path to a directory containing backups.
    #[arg(long)] dir: Option<PathBuf>,
}

fn main() -> Result<()> {
    let args = Cli::parse();

    env_logger::Builder::new()
        .filter_level(LevelFilter::Info)
        .parse_default_env()
        .init();

    if let Some(ref dir) = args.dir {
        fs::create_dir_all(dir)?;
    }

    let adb_exec = Exec::cmd("adb");
    let adb_exec = match args.device { None => adb_exec, Some(ref device) => adb_exec.arg("-d").arg(&device) };
    let adb_exec = match args.dir    { None => adb_exec, Some(ref dir) => adb_exec.cwd(dir) };

    let output = adb_exec.clone().args(&["shell", "dumpsys", "package", "packages"]).check_output()?.stdout_str();
    let packages = adb::parse_packages(&output)?;

    let user_packages: Vec<&Package> = packages.iter().filter(|pkg| !pkg.is_system).collect();
    let count = user_packages.len();
    for (i, pkg) in user_packages.iter().enumerate() {
        let from_path = format!("{0}/base.apk", pkg.code_path);
        let to_path = sanitise(&format!("{0}-{1}-{2}.apk", pkg.name, pkg.version_name, pkg.version_code));
        log::info!("Pulling {0}/{count}: {to_path}", i+1);
        adb_exec.clone().args(&["pull", &from_path, &to_path]).check_call()?;
    }
    Ok(())
}
