use anyhow::Result;
use pest::Parser;
use pest_derive::Parser;

#[derive(Parser)]
#[grammar = "packages.pest"]
pub struct PackagesParser;


#[derive(Debug)]
pub struct Package<'a> {
    pub name: &'a str,
    pub code_path: &'a str,
    pub version_code: i32,
    pub version_name: &'a str,
    pub is_system: bool,
}

pub fn parse_packages(content: &str) -> Result<Vec<Package>> {
    PackagesParser::parse(Rule::file, content)?
        .map(|package| {
            assert!(matches!(package.as_rule(), Rule::package));
            let mut name        : Option<&str> = None;
            let mut code_path   : Option<&str> = None;
            let mut version_code: Option<&str> = None;
            let mut version_name: Option<&str> = None;
            let mut is_system:    bool = false;
            for field in package.into_inner() {
                match field.as_rule() {
                    Rule::name        => { name         = Some(field.as_str()); }
                    Rule::codePath    => { code_path    = Some(field.as_str()); }
                    Rule::versionCode => { version_code = Some(field.as_str()); }
                    Rule::versionName => { version_name = Some(field.as_str()); }
                    Rule::minSdk      => {}
                    Rule::targetSdk   => {}
                    Rule::flags       => {
                        for flag in field.into_inner() {
                            assert!(matches!(flag.as_rule(), Rule::flag));
                            match flag.as_str() {
                                "SYSTEM" => { is_system = true; }
                                _ => {}
                            };
                        }
                    }
                    _ => unreachable!()
                };
            }
            Ok(Package {
                name:         name        .unwrap(),
                code_path:    code_path   .unwrap(),
                version_code: version_code.unwrap().parse()?,
                version_name: version_name.unwrap(),
                is_system:    is_system,
            })
        })
        .collect()
}
