use anyhow::{Error, Result};
use subprocess::{CaptureData, Exec, ExitStatus};

pub trait ExecExt {
    fn check_call(self) -> Result<ExitStatus>;
    fn check_output(self) -> Result<CaptureData>;
}

impl ExecExt for Exec {
    fn check_call(self) -> Result<ExitStatus> {
        let cmdline = self.to_cmdline_lossy();
        log::debug!("COMMAND: {cmdline}");
        let status = self.join()?;
        if status.success() {
            Ok(status)
        } else {
            Err(Error::msg(format!("Command `{cmdline}` finished with status \"{status:?}\".")))
        }
    }

    fn check_output(self) -> Result<CaptureData> {
        let cmdline = self.to_cmdline_lossy();
        log::debug!("COMMAND: {cmdline}");
        let capture = self.capture()?;
        let status = capture.exit_status;
        if status.success() {
            Ok(capture)
        } else {
            Err(Error::msg(format!("Command `{cmdline}` finished with status \"{status:?}\".")))
        }
    }
}
