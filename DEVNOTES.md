### Update dependencies

~~~sh
cargo update
~~~

### Build

~~~sh
cargo build
cargo build --release
~~~
